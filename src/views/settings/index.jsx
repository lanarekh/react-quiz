/**
 * External dependencies
 */
import React, { useState } from "react";

/**
 * Internal dependencies
 */
import CustomButton from "../../components/cutom-button";

/**
 * Style dependencies
 */
import './style.css';

export default function Settings(props) {
    const [mode, setMode] = useState(props.mode);
    
    const handleOptionChange = (e) => setMode(e.target.value);

    return (
        <div className="settings">
            <h2>Settings</h2>
            <div className="settings-radio">
                <label>
                    <input type="radio" name="mode" value="binary" checked={mode === 'binary'} onChange={handleOptionChange}/>
                    Binary
                </label>
                <label>
                    <input type="radio" name="mode" value="mult" checked={mode === 'mult'} onChange={handleOptionChange}/>
                    Multiple Choice
                </label>
            </div>
            <CustomButton text="Change" onClick={() => props.onChangeButton(mode)}/>
        </div>
    );
}