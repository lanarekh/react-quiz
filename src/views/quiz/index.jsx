/**
 * External dependencies
 */
import React, {useState, useEffect } from "react";

/**
 * Internal dependencies
 */

import QuoteDisplay from "../../components/quote-display";
import MultipleChoice from "../../components/multiple-choice";
import BinaryChoice from "../../components/binary-choice";
import CustomAlert from "../../components/custom-alert";

/**
 * Style dependencies
 */
import './style.css';

export default function Quiz(props) {
    const [showAlert, setShowAlert] = useState(false);
    useEffect(() => {
        if(props.alert && (props.alert.type === 'fail' || props.alert.type === 'success')){
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
                props.hideAlert();
            }, 1500);
        }
      }, [props]);
      
    return (
        <>
        {props.info && !showAlert
        ? <CustomAlert 
            text={props.info.text} 
            type={props.info.type}
            action={props.action}
            header={props.info.header}
        /> :
        <div className="quiz">
        <span>Who Said It?</span>
        <QuoteDisplay quote={props.quote}/>
        {showAlert 
        ? <CustomAlert text={props.alert.text} type={props.alert.type}/>   
        : (props.mode === 'binary')
        ? <BinaryChoice answer={props.answers} onAnswer={props.onAnswer}/>
        : <MultipleChoice answers={props.answers} onAnswer={props.onAnswer}/>
        }
        </div>
        }
        </>
    );
}