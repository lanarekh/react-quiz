/**
 * External dependencies
 */
import React from "react";

/**
 * Internal dependencies
 */

/**
 * Style dependencies
 */
import './style.css';

export default function MultipleChoice(props) {
    return (
        <ul className="multiple-choice">
            {Array.isArray(props.answers) ? props.answers.map((name,index) => (
                <li key={index} onClick={() => props.onAnswer(name)}>
                    -> {name}
                </li>
            )): null}
        </ul>
    );
}