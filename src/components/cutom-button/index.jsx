/**
 * External dependencies
 */
import React from "react";

/**
 * Internal dependencies
 */

/**
 * Style dependencies
 */
import './style.css';

export default function CustomButton(props) {
    let style = props.customStyle || "button-primary";
    return (
        <div className={"custom-button " + style} onClick={props.onClick}>
            {props.text}
        </div>
    );
}