/**
 * External dependencies
 */
import React from "react";

/**
 * Internal dependencies
 */
import CustomButton from "../cutom-button";

/**
 * Style dependencies
 */
import './style.css';

export default function CustomAlert(props) {
    return (
        <div className={"custom-alert"}>
            {props.header 
            ? <h2>{props.header}</h2>
            : null}
            <span className={"custom-alert-" + props.type}>
            {props.text}
            </span>
            {props.action
            ? <CustomButton text="Start Again" onClick={props.action}/>
            : null
            }
        </div>
    );
}