/**
 * External dependencies
 */
import React from "react";

/**
 * Internal dependencies
 */
import CustomButton from "../cutom-button";

/**
 * Style dependencies
 */
import './style.css';

export default function BinaryChoice(props) {
    return (
        <div className="binary-choice">
            <span>{props.answer || ""}</span>
            <div className="binary-choice-buttons">
                <CustomButton text="Yes" customStyle="button-yes" onClick={() => props.onAnswer(1)}/>
                <CustomButton text="No" customStyle="button-no" onClick={() => props.onAnswer(0)}/>
            </div>
        </div>
    );
}