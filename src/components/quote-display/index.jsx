/**
 * External dependencies
 */
import React from "react";

/**
 * Internal dependencies
 */

/**
 * Style dependencies
 */
import './style.css';

export default function QuoteDisplay(props) {
    return (
        <div className="quote-display">
            {props.quote || ""}
        </div>
    );
}