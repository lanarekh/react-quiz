/**
 * External dependencies
 */
import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

/**
 * Internal dependencies
 */

import Quiz from "./views/quiz";
import Settings from "./views/settings";
import Header from "./components/header";

/**
 * Style dependencies
 */
import './App.css';

function App() {
  const [mode, setMode] = useState('binary');
  const [newGame, setNewGame] = useState(false);
  const [info, setInfo] = useState(null);
  const [quizData, setQuizData] = useState({
    quote: null,
    answers: null,
    prevAnswer: null,
    result: null,
    questionNo: null,
  });
  const [alert, setAlert] = useState(null);

  const changeMode = (value) => {
    if(mode !== value){window.alert("Mode changed!");}
    setMode(value)
  };

  useEffect(() => {
    fetch(process.env.REACT_APP_API + '/quiz/new?mode=' + mode, {
        method: "POST",
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then((response) => {
    if(response.ok) {
        response.json().then((result) => {
          setQuizData({
            quote: result.quote,
            answers: result.answers,
            prevAnswer: result.prevAnswer,
            result: result.result,
            questionNo: result.questionNo,
          });
        });
      } else {
        setInfo({
          header: "Error",
          text: `There was an error from our server!`,
          type: 'info'
        });
      }
    }).catch(() => {
      setInfo({
        header: "Error",
        text: `There was an error with the connection!`,
        type: 'info'
      });
    });
  }, [mode, newGame]);

  const nextQuestion = (answer) => {
    fetch(process.env.REACT_APP_API + '/quiz/next?answer=' + encodeURIComponent(answer), {
      method: "POST",
      credentials: 'include',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then((response) => {
    if(response.ok) {
        response.json().then((result) => {
          if (quizData.result < result.result) {
            setAlert({
              text: 'Correct! The right answer is ' + result.prevAnswer,
              type: 'success',
              display: true,
            });
          } else {
            setAlert({
              text: 'Sorry, you are wrong! The right answer is ' + result.prevAnswer,
              type: 'fail',
              display: true,
            });
          }
          setQuizData({
            quote: result.quote,
            answers: result.answers,
            prevAnswer: result.prevAnswer,
            result: result.result,
            questionNo: result.questionNo,
          });
          if(!result.quote) {
            setInfo({
              header: "Statistics",
              text: `You've got ${result.result} out of ${result.questionNo}!`,
              type: 'info'
            });
          }
        });
      } else {
        setInfo({
          header: "Error",
          text: `There was an error from our server!`,
          type: 'info'
        });
      }
    }).catch(() => {
      setInfo({
        header: "Error",
        text: `There was an error with the connection!`,
        type: 'info'
      });
    });
  };

  return (
    <Router>
    <div className="App">
      <Header></Header>
      <Switch>
        <Route path="/settings">
          <Settings mode={mode} onChangeButton={changeMode} />
        </Route>
        <Route path="/">
          <Quiz 
            mode={mode} 
            quote={quizData.quote} 
            answers={quizData.answers} 
            onAnswer={nextQuestion} 
            prevAnswer={quizData.prevAnswer} 
            alert={alert} 
            info={info} 
            hideAlert={() => setAlert(null)}
            action={() => {setNewGame(!newGame); setInfo(null); setAlert(null);}}/>
        </Route>
      </Switch>
    </div>
  </Router>
  );
}

export default App;
